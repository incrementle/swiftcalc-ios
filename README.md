# SwiftCalc

## Description 
A minimalistic, usability-based scientific calculator, built for iOS.  
*Designed and developed by George Giovanis and Tanish Manku.*

## Flutter Version
An early prototype of SwiftCalc, built using Flutter, can be found here:  
https://gitlab.com/incrementle/swiftcalc
